var listArr = [];
function add_arr() {
  const numArr = document.getElementById("txt-num-arr").value * 1;
  listArr.push(numArr);
  console.log("listArr: ", listArr);
  document.getElementById("showArr").innerHTML = `<h2>[${listArr}]</h2>`;
}

function handle_1() {
  var numberIndex = listArr.length;
  var sum = 0;
  for (var i = 0; i < numberIndex; i++) {
    if (listArr[i] >= 0) {
      sum += listArr[i];
    }
  }
  document.getElementById(
    "result1"
  ).innerHTML = `<h1>Tổng số dương: ${sum} </h1>`;
}

function handle_2() {
  var count = 0;
  var numberIndex = listArr.length;
  for (var i = 0; i < numberIndex; i++) {
    if (listArr[i] > 0) {
      count++;
    }
  }
  document.getElementById(
    "result2"
  ).innerHTML = `<h1>Có tổng cộng: ${count} số dương</h1>`;
}

function handle_3() {
  var numberMin = Math.min.apply(null, listArr);
  document.getElementById(
    "result3"
  ).innerHTML = `<h1> Số nhỏ nhất là: ${numberMin} </h1>`;
}

function handle_4() {
  var newArr = [];
  for (var i = 0; i < listArr.length; i++) {
    if (listArr[i] >= 0) {
      newArr.push(listArr[i]);
    }
  }
  if (newArr.length != 0) {
    var numberMin = Math.min.apply(null, newArr);
    document.getElementById(
      "result4"
    ).innerHTML = `<h1> Số nhỏ nhất là: ${numberMin} </h1>`;
  } else {
    document.getElementById(
      "result4"
    ).innerHTML = `<h1> Mảng không có số dương </h1>`;
  }
}

function handle_5() {
  var soChanCuoiCung = -1;
  for (var i = 0; i < listArr.length; i++) {
    if (listArr[i] % 2 == 0 || listArr[i] % -2 == 0) {
      soChanCuoiCung = listArr[i];
    }
  }
  document.getElementById(
    "result5"
  ).innerHTML = `<h1> Số chẵn cuối cùng là ${soChanCuoiCung} </h1>`;
}

function handle_6() {
  var bienTam = 0;
  var vt1 = +document.getElementById("txt-vt1").value;
  var vt2 = +document.getElementById("txt-vt2").value;
  bienTam = listArr[vt1];
  listArr[vt1] = listArr[vt2];
  listArr[vt2] = bienTam;
  document.getElementById(
    "result6"
  ).innerHTML = `<h1> Mảng sau khi đổi chổ: ${listArr} </h1>`;
}

function handle_7() {
  document.getElementById(
    "result7"
  ).innerHTML = `<h1> Mảng sau khi sắp xếp: ${listArr.sort(function (a, b) {
    return a - b;
  })} </h1>`;
}

function handle_8() {
  var soNguyenToDauTien = -1;
  var count = 0;
  for (var i = 0; i < listArr.length; i++) {
    for (var j = 2; j <= Math.sqrt(listArr[i]); j++) {
      if (listArr[i] % j == 0) {
        count++;
        break;
      }
    }
    if (count == 0) {
      soNguyenToDauTien = listArr[i];
      break;
    } else {
      count = 0;
    }
  }
  document.getElementById(
    "result8"
  ).innerHTML = `<h1> ${soNguyenToDauTien} </h1>`;
}
listArrBt9 = [];
function add_arr_bt9() {
  var numArrBt9 = document.getElementById("txt-num-arr-bt9").value * 1;
  listArrBt9.push(numArrBt9);
  document.getElementById("showArrBt9").innerHTML = `<h1>[${listArrBt9}]</h1>`;
}
function handle_9() {
  var count = 0;
  if (listArrBt9.length == 0) {
    document.getElementById(
      "result9"
    ).innerHTML = `<h1> Có ${count} số nguyên</h1>`;
  } else {
    for (var i = 0; i < listArrBt9.length; i++) {
      if (Number.isInteger(listArrBt9[i]) == true) {
        count++;
      }
    }
    document.getElementById(
      "result9"
    ).innerHTML = `<h1> Có ${count} số nguyên</h1>`;
  }
}

function handle_10() {
  var demSoChan = 0;
  var demSoLe = 0;
  for (var i = 0; i < listArr.length; i++) {
    if (listArr[i] % 2 == 0) {
      demSoChan++;
    } else {
      demSoLe++;
    }
  }
  if (demSoChan == demSoLe) {
    document.getElementById("result10").innerHTML = `<h1>Số chẵn = Số lẻ</h1>`;
  } else if (demSoChan > demSoLe) {
    document.getElementById("result10").innerHTML = `<h1>Số chẵn > Số lẻ</h1>`;
  } else {
    document.getElementById("result10").innerHTML = `<h1>Số chẵn < Số lẻ</h1>`;
  }
}
